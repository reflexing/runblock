## Точечный обход блокировок Роскомнадзора на роутерах с OpenWrt/LEDE с помощью Linux Policy Routing

### Принцип работы
Файл `/etc/config/runblock/runblock.sh` отвечает за загрузку списков IP, заблокированных РКН и создаёт наборы IP.
Файл `/etc/hotplug.d/96-runblock` запускает `runblock.sh` при поднятии интерфейса WAN основного интернет-соединения.

В файле `/etc/openvpn/runblock/upvpn` происходит основная магия роутинга на основании политик.
Пакеты, адреса назначения которых находятся в наборах IP, которые были созданы скриптом `runblock.sh`, помечаются меткой.

Дополнительно, меткой помечаются все DNS-запросы. Если этот функционал вам не нужен или ваш провайдер не подменяет DNS сервера, 
в файле `runblock.sh` закомментируйте строчки, помеченные комментарием *Mark DNS port*.

Все помеченные этой меткой пакеты перенаправляются через ранее созданную таблицу маршрутизации с помощью DNAT в наш OpenVPN тоннель.
Файл `/etc/openvpn/runblock/downvpn` сбрасывает изменения, созданные скриптом `upvpn`, при потере OpenVPN тоннеля.

### Установка и настройка

1. Скопируйте все файлы из каталога *etc* этого репозитория на ваш роутер.
2. Установите `ipset`:

    `opkg update`  
    `opkg install ipset`  
    
3. Загрузка списков заблокированных IP с серверов Роскомсвободы

    *wget* в **OpenWRT** по умолчанию предоставляется *busybox* и не поддерживает ни SSL, ни перенаправления.
Поэтому для загрузки списков заблокированных IP с серверов Роскомсвободы по умолчанию в файле `runblock.sh` используется HTTP зеркало Роскомсвободы.

    Если вы хотите использовать HTTPS зеркала, установите полноценный *wget* или *curl* и корневые сертификаты:

    * Для установки *wget*: `opkg install wget`
    * Для *curl*: `opkg install curl`
    * Корневые сертификаты: `opkg install ca-certificates`

    И раскомментируйте соответствующие строчки в *runblock.sh*.
    
4. Проверка таблиц:

    `ipset list -terse`

5. Установите *iproute2*

    `opkg install ip`
    
    Добавьте новую таблицу маршрутизации, через которую пойдёт трафик на адреса, заблокированные РКН:
   
    `echo 99 vpn >> /etc/iproute2/rt_tables`  
    Для проверки: `cat /etc/iproute2/rt_tables`
    
6. Периодическая загрузка заблокированных IP
    
    Включите *cron*: `/etc/init.d/cron enable`

    Для загрузки каждые 6 часов добавьте в `crontab`:

        :::shell
        root@openwrt:~# cat >> /etc/crontab << EOF
        0 */6 * * * /etc/config/runblock/runblock.sh
        EOF
        
    Перезагрузите *cron*: `/etc/init.d/cron restart`

#### Настройка интерфейса для OpenVPN

При каждом включении роутера должен быть создан интерфейс
`tun1337`, использующийся OpenVPN (создаём с псевдонимом `runblock`):

```shell
root@openwrt:~# cat >> /etc/config/network << EOF
config interface 'runblock'
  option ifname 'tun1337'
  option proto 'none'
EOF
```

#### Настройка межсетевого экрана
Теперь созданный интерфейс `runblock` можно использовать в зонах межсетевого экрана:

```shell
root@openwrt:~# cat >> /etc/config/firewall << EOF
config zone
  option name 'runblock'
  option input 'REJECT'
  option output 'ACCEPT'
  option forward 'REJECT'
  option masq '1'
  option mtu_fix '1'
  option network 'runblock'

config forwarding
  option dest 'runblock'
  option src 'lan'
EOF
```

Создание наборов IP:

```shell
root@openwrt:~# cat >> /etc/config/firewall << EOF
config ipset
  option external 'vpn-whitelist’
  option match 'src_net’
  option family 'ipv4’
  option storage ‘hash’
  option maxelem 1000000

config ipset
  option external 'vpn-whitelist-tmp’
  option match 'src_net’
  option family 'ipv4’
  option storage ‘hash’
  option maxelem 1000000
EOF
```