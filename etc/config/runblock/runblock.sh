#!/bin/sh
TARGET_SET=vpn-whitelist
TARGET_TMP=vpn-whitelist-tmp
IPSET_FILE=/tmp/blacklist.ipset

function ipset_set
{
	ipset swap ${TARGET_TMP} ${TARGET_SET}
	ipset save ${TARGET_SET} > ${IPSET_FILE}
}

ipset destroy -q ${TARGET_TMP}
ipset create -q ${TARGET_SET} -exist hash:ip maxelem 100000
ipset create ${TARGET_TMP} -exist hash:ip maxelem 100000

if [ -s $IPSET_FILE ] # file is not zero-size
then
  logger -s -t runblock "Saved ipset exists, restoring…"
  ipset restore -exist -q < $IPSET_FILE
  ipset_set
else
  logger -s -t runblock "No saved ipset, downloading…"
fi

wget -O - http://api.reserve-rbl.ru/api/ips | \
  awk '{gsub(/"/,"",$1); gsub(";"," ",$1); print $1}' | \
#  tee blacklist.txt | \
  xargs -n1 ipset add -exist ${TARGET_TMP}

ipset_set
ipset destroy ${TARGET_TMP}
